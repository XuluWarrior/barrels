const ws = require("nodejs-websocket");

const players = [];

let inPlay = false;
let nextPlayer = 1;
let nextNum = 1;

function numberToCorrectResponse(num) {
    if (num % 5 === 0 && num % 7 === 0) {
        return "barrels and casks"
    } else if (num % 5 === 0) {
        return "casks";
    } else if (num % 7 === 0) {
        return "barrels";
    } else {
        return "" + num;
    }
}

// Scream server example: "hi" -> "HI!!!"
const server = ws.createServer(function (conn) {
    console.log("New connection");

    let currentPlayer;
    if (inPlay) {
        conn.sendText(`Sorry.  Game has already started`);
    } else {
        const playerNumber = players.length + 1;
        conn.sendText(`Welcome player ${playerNumber}`);
        currentPlayer = {
            num: playerNumber,
            conn,
            active: true
        };
        players.push(currentPlayer);
    }

    conn.on("text", function (str) {
        if (!inPlay) {
            broadcast(`Player ${currentPlayer.num}: ${str}`);
            if (str.toLowerCase() === "start") {
                inPlay = true;
            } else {
                // Ignore idle chat
            }
            return;
        }

        if (currentPlayer.active) {
            broadcast(`Player ${currentPlayer.num}: ${str}`);
            if (currentPlayer.num === nextPlayer) {
                const correctResponse = numberToCorrectResponse(nextNum);
                if (str.toLowerCase() === correctResponse) {
                    // Do nothing
                } else {
                    currentPlayer.isActive = false;
                    broadcast("WRONG!! " + correctResponse + "!!!");
                }
                nextNum++;
                nextPlayer = nextPlayer === players.length ? 1 : nextPlayer + 1;

            } else {
                if (currentPlayer.isActive) {
                    currentPlayer.isActive = false;
                    broadcast(`Player ${currentPlayer.num} spoke out of turn.  They're out!!!`);
                } else {
                    conn.sendText("Be quiet!  You're out of the game!!!!");
                }
            }
        } else {

            conn.sendText("Be quiet! You're out of this game");
        }
    });

    conn.on("close", function (code, reason) {
        console.log("Connection closed")
    });

}).listen(8001);

function broadcast(msg) {
    server.connections.forEach(function (conn) {
        conn.sendText(msg)
    })
}