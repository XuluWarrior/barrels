# Barrels and Casks

**A Websockets server with Electron/Browser frontend to play the game of Barrels and Casks**

## To run server
```bash
cd Service
# Install dependencies
npm install
# Run the app
npm start
```

## To run Electron app
```bash
cd UI
# Install dependencies
npm install
# Run the app
npm start
```

## To build www app and Electron binaries
```bash
npm run package:all
# Browser files are built to www folder
# Electron binaries are built to dist
```

## Addendum
The project also includes a typeahead UI component (unused)