'use strict';

const browserify = require('browserify');
const gulp = require('gulp');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const gutil = require('gulp-util');
const babelify = require('babelify');
const envify = require('envify');
const gulpEnvify = require('gulp-envify'); // envify without browserifying
const runSequence = require('run-sequence');

const del = require('del');
const replace = require('gulp-string-replace');

const uglifyJS = require('uglify-es');
const uglifyComposer = require('gulp-uglify/composer');
const minify = uglifyComposer(uglifyJS);

const buildDir = "electron-dist";
const mainDir = buildDir + "/main";
const appDir = "app";
const wwwDir = "www";

gulp.task('clean', function (cb) {
    del([buildDir, 'release'], cb);
});


const fs = require('fs');

const NODE_MODULES = fs.readdirSync("./node_modules");


gulp.task('www:bundle', function () {
    const b = browserify({
        entries: './src/index.js',
        debug: false,
        extensions: [ ".jsx" ],
        transform: [[babelify, {presets: ['react']}], envify]
    });

    return b.bundle()
        .pipe(source('index.js'))
        .pipe(buffer())
//        .pipe(minify({}))
        .pipe(gulp.dest(wwwDir));
});

gulp.task('app:bundle', function () {
    const b = browserify({
        entries: './src/index.js',
        debug: false,
        node: true,
        bundleExternal: true,
        builtins: true,
        detectGlobals: false,
        extensions: [ ".jsx" ],
        transform: [[babelify, {presets: ['react']}], envify]
    }).external("electron");

    return b.bundle()
        .pipe(source('index.js'))
        .pipe(buffer())
//        .pipe(minify({}))
        .pipe(gulp.dest(appDir));
});


gulp.task('www:html', function () {
    return gulp.src('./src/index.html')
        .pipe(gulp.dest(wwwDir));
});

gulp.task('app:html', function () {
    return gulp.src('./src/index.html')
        .pipe(gulp.dest(appDir));
});

gulp.task('www:css', function () {
    return gulp.src('./src/css/*', { "base" : "./src" })
        .pipe(gulp.dest(wwwDir));
});

gulp.task('app:css', function () {
    return gulp.src('./src/css/*', { "base" : "./src" })
        .pipe(gulp.dest(appDir));
});


gulp.task('www', ['www:bundle', 'www:html', 'www:css']);
gulp.task('app', ['app:bundle', 'app:html', 'app:css']);

gulp.task('default', ['www', 'app']);