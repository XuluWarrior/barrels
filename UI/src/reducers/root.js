const { combineReducers } = require('redux');

const game = require("./game").reducer;

module.exports = combineReducers({
    game
});