


const Immutable = require('immutable');

const {createPromiseThunk, helpers: {immutable: {AsyncActionStatusRecord, reduceAsyncAction}}} = require('redux-promise-thunk');

const axios = require('axios');

const Episodes = function() {

    const serverURL = process.env.HOST ||
        "ws://localhost:8001";

    let ws;

    this.StateRecord = Immutable.Record({
        connection: null,
        connectStatus: new AsyncActionStatusRecord(),
        sendMessageStatus: new AsyncActionStatusRecord(),
        message: "",
        messages: []
    }, 'Episodes.StateRecord');

    const initialState = new this.StateRecord();


    this.asyncActionTypes = {
        connect: "CONNECT",
        sendMessage: "SEND_MESSAGE"
    };

    this.actionTypes = {
        setMessage: "SET_MESSAGE",
        addMessage: "ADD_MESSAGE"
    };

    this.reducer = (state = initialState, action = null) => {
        if (action.meta && action.meta.asyncType) {
            switch (action.meta.asyncType) {
                case this.asyncActionTypes.connect:
                    return reduceAsyncAction(state, action, ["connectStatus"]);
                default:
                    return state;
            }
        } else {
            switch (action.type) {
                case this.actionTypes.setMessage:
                    return state.set("message", action.message);
                case this.actionTypes.addMessage:
                    const newMessages = state.messages.slice();
                    newMessages.push(action.message);
                    console.log(newMessages);
                    return state.set("messages", newMessages);
                default:
                    return state;
            }
        }
    };

    const connect = createPromiseThunk(this.asyncActionTypes.connect, (arg, dispatch) => {
        return new Promise((resolve, reject) => {
            ws = new WebSocket(serverURL);

            ws.onopen = function open() {
                resolve();
            };

            ws.onmessage = (message) => {
                dispatch(this.actions.addMessage(message.data));
            };
        })
    });

    const sendMessage = createPromiseThunk(this.asyncActionTypes.sendMessage, function(message) {
        return new Promise((resolve, reject) => {
            ws.send(message);
            resolve();
        })
    });

    this.actions = {
        connect,
        setMessage: (message) => {
            return {
                type: this.actionTypes.setMessage,
                message
            }
        },
        addMessage: (message) => {
            return {
                type: this.actionTypes.addMessage,
                message
            }
        },
        sendMessage
    };
};

module.exports = new Episodes();