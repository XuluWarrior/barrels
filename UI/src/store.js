const { createStore, compose, applyMiddleware } = require('redux');
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const thunk = require('redux-thunk').default;

const rootReducer = require('./reducers/root');

const store = createStore(
    rootReducer,
    composeEnhancers(
        applyMiddleware(
            thunk
        )
    )
);

module.exports = store;