const React = require('react');
const ReactDOM = require('react-dom');

const {Provider} = require('react-redux');
const store = require('./store');

const Main = require('./components/main.jsx');

ReactDOM.render(
    <Provider store={store}>
        <Main />
    </Provider>,
    document.getElementById('root'));