const React = require('react');
const { Component } = React;

const {bindActionCreators} = require('redux');
const { connect } = require('react-redux');

const game = require('../reducers/game');

const Game = require('./game');

const App = require('grommet/components/App');

class Main extends Component {

    componentDidMount() {
        this.props.actions.connect();
    }


    render() {
        return <App centered={false}><Game /></App>;
    }
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(game.actions, dispatch)
    }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Main);