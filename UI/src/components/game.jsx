const React = require('react');
const { Component } = React;

const {bindActionCreators} = require('redux');
const { connect } = require('react-redux');

const gameActions = require('../reducers/game').actions;

const {helpers: {immutable: {actionCompletedSuccessfully, actionError}}} = require('redux-promise-thunk');

const Article = require('grommet/components/Article');
const Button = require('grommet/components/Button');
const TextInput = require('grommet/components/TextInput');

class MessageBox extends Component {
    onDOMChanged(e) {
        this.props.setMessage(e.target.value);
    }

    render() {
        return <TextInput onDOMChange={this.onDOMChanged.bind(this)} value={this.props.message}/>;
    }
}
class Game extends Component {

    render() {
        const {connectStatus, message}  = this.props;
        const connected = actionCompletedSuccessfully(connectStatus) ? "Connected" : "Connecting";
        return <Article>
            {connected}
            <MessageBox setMessage={this.props.actions.setMessage} message={message}/>
            <Button label={"Send"} onClick={() => {this.props.actions.sendMessage(message)}}/>
        </Article>
    }
}


function mapStateToProps(state) {
    return {
        connectStatus: state.game.connectStatus,
        message: state.game.message
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(gameActions, dispatch)
    }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Game);