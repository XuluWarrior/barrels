const React = require('react');
const { Component } = React;

const faker = require('faker');
const fuzzysearch = require('fuzzysearch');

const {bindActionCreators} = require('redux');
const { connect } = require('react-redux');


const Article = require('grommet/components/Article');
const TextInput = require('grommet/components/TextInput');
const List = require('grommet/components/List');
const ListItem = require('grommet/components/ListItem');

class TypeAhead extends Component {
    constructor() {
        super();
        const names = new Array(1000);
        for (let i=0; i< names.length; i++) {
            names[i] = faker.fake("{{name.firstName}} {{name.lastName}}");
        }
        this.state = {
            searchText: "",
            names,
            searchResults: []
        }

//        this.onSearchChanged = this.onSearchChanged.bind(this);
    }

    onSearchChanged(e){
        const searchText = e.target.value;
        const lowerCaseSearch = searchText.toLowerCase();

        const searchResults = this.state.names.filter(name => {
            return fuzzysearch(lowerCaseSearch, name.toLowerCase());
        });

        this.setState({
            searchText,
            searchResults
        });
    }

    render() {
        const {searchText, searchResults} = this.state;

        const listItems = searchText ?
            searchResults.length ?
                searchResults.slice(0, 10).map((result, index) => <ListItem key={index}>{result}</ListItem>)
                : "No results"
            : "";
        return <Article>
            <TextInput value={searchText} onDOMChange={this.onSearchChanged.bind(this)}/>
            <List>{listItems}</List>
        </Article>
    }
}

module.exports = TypeAhead;